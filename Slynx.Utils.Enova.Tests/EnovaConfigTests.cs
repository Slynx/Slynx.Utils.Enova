// 
// EnovaConfigTests.cs is a part of Slynx.Utils.Enova.Tests project.
// 
// Created by Slynx on 08/02/2020 19:43.
// Last modified on 09/02/2020 18:49

using System;
using NUnit.Framework;
using Soneta.Business;
using Soneta.Kadry;
using Soneta.Test;
using Soneta.Types;

namespace Slynx.Utils.Enova.Tests
{
    [TestDatabase("UnitTest")]
    [TestFixture]
    public class EnovaConfigTests : DbTransactionTestBase
    {
        private class TestConfig : EnovaConfig
        {
            public TestConfig(Session session) : base(session, "Test")
            {
            }

            public T GetValue<T>(String prop)
            {
                return base.GetValue<T>(prop);
            }

            public new void SetValue<T>(String prop, T value)
            {
                base.SetValue(prop, value);
            }
        }

        [TestCase("Test1", AttributeTargets.Assembly, ExpectedResult = AttributeTargets.Assembly)]
        [TestCase("Test2", new Byte[] { 12, 15 }, ExpectedResult = new Byte[] { 12, 15 })]
        [Test]
        public T Test1<T>(String property, T value)
        {
            using (var session = Login.CreateSession(false, true))
            using (session.Logout(true))
            {
                var config = new TestConfig(session);
                config.SetValue(property, value);
                return config.GetValue<T>(property);
            }
        }

        [Test]
        public void TestOther()
        {
            Assert.AreEqual(Test1("Test3", MemoBin.Empty).Length, MemoBin.Empty.Length);
            var row = (Pracownik)KadryModule.GetInstance(Session).Pracownicy.DefaultViewKey.GetNext();

            Assert.AreEqual(Test1("Test4", row).Guid, row.Guid);

            Assert.AreEqual(Test1("Test5", new Percent(0.23m)), new Percent(0.23m));

            var text = (MemoText)"123";
            Assert.AreEqual(Test1("Test6", text), text);
        }
    }
}