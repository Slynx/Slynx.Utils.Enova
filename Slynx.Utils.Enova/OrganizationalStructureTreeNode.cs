// 
// OrganizationalStructureTreeNode.cs is a part of Slynx.Utils.Enova project.
// 
// Created by Slynx on 14/10/2019 16:09.
// Last modified on 14/10/2019 16:18

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Soneta.Business;
using Soneta.Business.Db;
using Soneta.Core;
using Soneta.Types;

namespace Slynx.Utils.Enova
{
    public class OrganizationalStructureTreeNode<TValue> : IEnumerable<OrganizationalStructureTreeNode<TValue>> where TValue : IŹródłoPowiązaniaStrukturyOrganizacyjnej
    {
        public OrganizationalStructureTreeNode(ElementStrukturyOrganizacyjnej item, OrganizationalStructureTree<TValue> root)
        {
            Source = item;
            Root = root;
        }

        public OrganizationalStructureTreeNode<TValue> this[String code, DefinicjaElementuStrukturyOrganizacyjnej def = null]
        {
            get { return this[code, String.Empty, def]; }
        }

        public OrganizationalStructureTreeNode<TValue> this[String code, String name, DefinicjaElementuStrukturyOrganizacyjnej def = null]
        {
            get
            {
                var element = GetNode(code, name);
                if (element == null)
                {
                    return AddNode(code, name, def ?? Root.Definition);
                }

                return element;
            }
        }

        private OrganizationalStructureTreeNode<TValue>[] Nodes
        {
            get
            {
                //Source[Root.HistoryDate] is bugged (enova 12.3)
                return Source.WszystkieElementy.Cast<ElementStrukturyOrganizacyjnej>().Where(s => Root.HistoryDate.IsNull || s.Okres.Contains(Root.HistoryDate))
                    .Select(s => new OrganizationalStructureTreeNode<TValue>(s, Root)).ToArray();
            }
        }

        public OrganizationalStructureTreeNodeValue<TValue>[] Connections
        {
            get { return Source.Powiązania.Select(s => new OrganizationalStructureTreeNodeValue<TValue>(this, s)).ToArray(); }
        }

        public ElementStrukturyOrganizacyjnej Source { get; }

        public OrganizationalStructureTreeNode<TValue> Parent
        {
            get { return Source.Nadrzedny != null ? new OrganizationalStructureTreeNode<TValue>(Source.Nadrzedny, Root) : null; }
        }

        public OrganizationalStructureTree<TValue> Root { get; protected set; }

        public Boolean CanDelete
        {
            get { return Source.GetCanDelete() && !Source.IsDeleted; }
        }

        public Int32 Count
        {
            get { return Nodes.Length; }
        }

        IEnumerator<OrganizationalStructureTreeNode<TValue>> IEnumerable<OrganizationalStructureTreeNode<TValue>>.GetEnumerator()
        {
            return Nodes.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<OrganizationalStructureTreeNode<TValue>>)this).GetEnumerator();
        }

        public Boolean SetConnection(TValue value)
        {
            if (ConnectionExists(value))
            {
                return false;
            }

            var powiazanie = new PowiązanieStrukturyOrganizacyjnej(Source, value);
            using (ITransaction tr = Source.Session.Logout(true))
            {
                Source.Module.PowiazaniaStrOrg.AddRow(powiazanie);
                tr.CommitUI();
            }

            return true;
        }

        public OrganizationalStructureTreeNodeValue<TValue> GetConnection(String sourceName)
        {
            var items = Source.Powiązania.Where(s => s.Zrodlo.Nazwa == sourceName).ToArray();
            if (items.Length > 1)
            {
                throw new ArgumentException($"Ambiguous match with name \'{sourceName}\' found. {items.Length} items, expected 1");
            }

            return items.Length > 0 ? new OrganizationalStructureTreeNodeValue<TValue>(this, items[0]) : null;
        }

        public OrganizationalStructureTreeNodeValue<TValue> GetConnection(TValue value)
        {
            var items = Source.Powiązania.Where(s => s.Zrodlo == (IŹródłoPowiązaniaStrukturyOrganizacyjnej)value).ToArray();
            if (items.Length > 1)
            {
                throw new ArgumentException($"Ambiguous match with value \'{value?.Nazwa}\' found. {items.Length} items, expected 1");
            }

            return items.Length > 0 ? new OrganizationalStructureTreeNodeValue<TValue>(this, items[0]) : null;
        }

        public OrganizationalStructureTreeNode<TValue> AddNode(String code, String name, DefinicjaElementuStrukturyOrganizacyjnej definition = null)
        {
            if (Root == null)
            {
                throw new InvalidOperationException("Node is not attachted to Tree");
            }

            if (Root.IsSealed)
            {
                throw new InvalidOperationException("Cannot modify sealed tree");
            }

            if (definition == null)
            {
                throw new NullReferenceException("Organization structure element definition cannot be null");
            }

            var element = new ElementStrukturyOrganizacyjnej(Source);
            using (ITransaction tr = Source.Session.Logout(true))
            {
                Source.Module.ElementyStrOrg.AddRow(element);
                element.Definicja = definition;
                element.Kod = code;
                element.Nazwa = String.IsNullOrWhiteSpace(name) ? code : name;
                if (Root.Source.Struktura.Historycznosc)
                {
                    UpdateOkres(element, new FromTo(Root.HistoryDate, Date.MaxValid));
                }

                tr.CommitUI();
            }

            return new OrganizationalStructureTreeNode<TValue>(element, Root);
        }

        private OrganizationalStructureTreeNode<TValue> GetNode(String code, String name)
        {
            return Nodes.FirstOrDefault(innerItem =>
                String.Equals(innerItem.Source.Kod.Trim(), code.Trim(), StringComparison.InvariantCultureIgnoreCase) ||
                String.Equals(innerItem.Source.Nazwa.Trim(), name.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public Boolean Contains(String code)
        {
            return Contains(code, String.Empty);
        }

        public Boolean Contains(String code, String name)
        {
            return GetNode(code, name) != null;
        }

        public Boolean ConnectionExists(TValue value)
        {
            return GetConnection(value) != null;
        }

        public void Delete()
        {
            if (Parent == null)
            {
                throw new Exception("Root cannot be removed");
            }

            foreach (var node in Nodes)
            {
                node.Delete();
            }

            if (Source.Struktura.Historycznosc)
            {
                if (Source.Okres.To > Date.Today)
                {
                    using (ITransaction tr = Source.Session.Logout(true))
                    {
                        UpdateOkres(Source, new FromTo(Source.Okres.From, Date.Today));
                        tr.CommitUI();
                    }
                }

                return;
            }

            foreach (var value in Connections.Where(value => value.CanDelete))
            {
                value.Delete();
            }

            using (ITransaction tr = Source.Session.Logout(true))
            {
                foreach (Attachment attachment in Source.Attachments.Cast<Attachment>().ToArray())
                {
                    attachment.Delete();
                }

                if (!Source.IsDeleted)
                {
                    Source.Delete();
                }

                tr.CommitUI();
            }
        }

        public void UpdateHistoryPeriod(FromTo period)
        {
            using (var tr = Source.Session.Logout(true))
            {
                UpdateOkres(Source, period);
                tr.CommitUI();
            }
        }

        private void UpdateOkres(ElementStrukturyOrganizacyjnej element, FromTo period)
        {
            MethodInfo methodInfo = typeof(ElementStrukturyOrganizacyjnej).GetMethod("UpdateOkres", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            methodInfo.Invoke(element, new Object[] { period });
        }
    }
}