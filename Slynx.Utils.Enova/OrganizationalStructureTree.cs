// 
// OrganizationalStructureTree.cs is a part of Slynx.Utils.Enova project.
// 
// Created by Slynx on 14/10/2019 16:09.
// Last modified on 14/10/2019 16:18

using System;
using Soneta.Business;
using Soneta.Core;
using Soneta.Types;

namespace Slynx.Utils.Enova
{
    public class OrganizationalStructureTree<TValue> : OrganizationalStructureTreeNode<TValue> where TValue : IŹródłoPowiązaniaStrukturyOrganizacyjnej
    {
        public OrganizationalStructureTree(StrukturaOrganizacyjna structure) : base(structure.Root, null)
        {
            Root = this;
        }

        public Date HistoryDate { get; set; } = Date.Today;

        public Boolean IsSealed { get; set; }

        public DefinicjaElementuStrukturyOrganizacyjnej Definition { get; set; }
    }
}