// 
// OrganizationalStructureTreeNodeExtensions.cs is a part of Slynx.Utils.Enova project.
// 
// Created by Slynx on 21/01/2018 18:13.
// Last modified on 16/10/2019 00:56

using System.Collections.Generic;
using Soneta.Business;

namespace Slynx.Utils.Enova.ExtensionMethods
{
    public static class OrganizationalStructureTreeNodeExtensions
    {
        /// <summary>
        ///     Returns all connections from specific node and below.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static OrganizationalStructureTreeNodeValue<T>[] GetConnections<T>(this OrganizationalStructureTreeNode<T> node) where T : IŹródłoPowiązaniaStrukturyOrganizacyjnej
        {
            if (node == null)
            {
                return new OrganizationalStructureTreeNodeValue<T>[0];
            }

            var list = new List<OrganizationalStructureTreeNodeValue<T>>();

            list.AddRange(node.Connections);

            foreach (OrganizationalStructureTreeNode<T> treeNode in node)
            {
                list.AddRange(GetConnections(treeNode));
            }

            return list.ToArray();
        }

        /// <summary>
        ///     Returns all nodes from specific node and below.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static OrganizationalStructureTreeNode<T>[] GetNodes<T>(this OrganizationalStructureTreeNode<T> node) where T : IŹródłoPowiązaniaStrukturyOrganizacyjnej
        {
            if (node == null)
            {
                return new OrganizationalStructureTreeNode<T>[0];
            }

            var list = new List<OrganizationalStructureTreeNode<T>>();

            list.AddRange(node);

            foreach (OrganizationalStructureTreeNode<T> treeNode in node)
            {
                list.AddRange(GetNodes(treeNode));
            }

            return list.ToArray();
        }
    }
}