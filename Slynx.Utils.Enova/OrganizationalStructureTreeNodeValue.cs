// 
// OrganizationalStructureTreeNodeValue.cs is a part of Slynx.Utils.Enova project.
// 
// Created by Slynx on 14/10/2019 16:09.
// Last modified on 14/10/2019 16:18

using System;
using Soneta.Business;
using Soneta.Core;

namespace Slynx.Utils.Enova
{
    public class OrganizationalStructureTreeNodeValue<TValue> where TValue : IŹródłoPowiązaniaStrukturyOrganizacyjnej
    {
        public OrganizationalStructureTreeNodeValue(OrganizationalStructureTreeNode<TValue> parent, PowiązanieStrukturyOrganizacyjnej connection)
        {
            Parent = parent;
            Source = connection;
        }

        public Boolean CanDelete
        {
            get { return Source.GetCanDelete(); }
        }

        public TValue Value
        {
            get { return (TValue)Source.Zrodlo; }
        }

        public OrganizationalStructureTreeNode<TValue> Parent { get; }

        public PowiązanieStrukturyOrganizacyjnej Source { get; }

        public void Delete()
        {
            using (ITransaction tr = Source.Session.Logout(true))
            {
                Source.Delete();
                tr.CommitUI();
            }
        }

        public static explicit operator TValue(OrganizationalStructureTreeNodeValue<TValue> value)
        {
            return value.Value;
        }

        public static explicit operator PowiązanieStrukturyOrganizacyjnej(OrganizationalStructureTreeNodeValue<TValue> value)
        {
            return value.Source;
        }
    }
}