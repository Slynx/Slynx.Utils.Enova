﻿// 
// EnovaConfig.cs is a part of Slynx.Utils.Enova project.
// 
// Created by Slynx on 08/02/2020 19:30.
// Last modified on 09/02/2020 18:47

using System;
using System.Linq;
using System.Reflection;
using Soneta.Business;
using Soneta.Config;

namespace Slynx.Utils.Enova
{
    /// <summary>
    ///     The extended <see cref="CfgManager" /> implementation as a abstract class for developed extension.
    /// </summary>
    public abstract class EnovaConfig : ISessionable
    {
        private static readonly MethodInfo typeToEnumMethodInfo = typeof(CfgAttribute).GetMethod("TypeToEnum", BindingFlags.Static | BindingFlags.NonPublic);

        private readonly Session session;

        /// <summary>
        ///     Default constructor requires <see cref="Session" /> object.
        ///     The path array describes nodes connection when data is stored in xml file. Example path can looks like,
        ///     <code> new { "Slynx", "MyExtension" } </code> or
        ///     <code> new { "Slynx", "MyExtension", "BigConfigPartOfMyExtension" }</code>.
        ///     Cannot be empty.
        /// </summary>
        /// <param name="session">The <see cref="Session" /> object. Session must be created with 'config' parameter set to true</param>
        /// <param name="path"></param>
        public EnovaConfig(Session session, params String[] path)
        {
            if (path?.All(String.IsNullOrWhiteSpace) != false)
            {
                throw new ArgumentException($"{nameof(path)} cannot be null or empty");
            }

            Path = path;
            this.session = session ?? throw new ArgumentNullException(nameof(session));
            var cfgManager = new CfgManager(session);
            Root = cfgManager.Root;
        }

        /// <summary>
        ///     Path created for config. Passed through constructor. Cannot be changed.
        /// </summary>
        protected String[] Path { get; }

        /// <summary>
        ///     Root <see cref="CfgNode" />. Shouldn't be modified or used directly.
        /// </summary>
        protected CfgNode Root { get; }

        /// <summary>
        ///     The <see cref="Session" /> object, cannot be changed or modified. It's used by internal <see cref="CfgManager" />.
        /// </summary>
        Session ISessionable.Session
        {
            get { return session; }
        }

        protected CfgNode GetNode()
        {
            using (ITransaction transaction = session.Logout(true))
            {
                CfgNode node = Root;
                for (var i = 0; i < Path.Length; i++)
                {
                    node = node.FindSubNode(Path[i], false) ?? node.AddNode(Path[i], i + 1 == Path.Length ? CfgNodeType.Leaf : CfgNodeType.Node);
                }

                transaction.Commit();
                return node;
            }
        }

        /// <summary>
        ///     Set value for new or existing member in config.
        ///     The type of value and it's internal representation is identified by generic type.
        ///     Every change is prepared in separate transaction.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        protected void SetValue<T>(String name, T value)
        {
            using (ITransaction transaction = session.Logout(true))
            {
                CfgNode node = Root;
                for (var i = 0; i < Path.Length; i++)
                {
                    node = node.FindSubNode(Path[i], false) ?? node.AddNode(Path[i], i + 1 == Path.Length ? CfgNodeType.Leaf : CfgNodeType.Node);
                }

                // Get attribute and process value for some specific types.
                (AttributeType attributeType, Object output) = GetAndProcessAttributeType(value, value);

                CfgAttribute attribute = node.FindAttribute(name, false);
                if (attribute != null)
                {
                    attribute.Value = output;
                }
                else
                {
                    node.AddAttribute(name, attributeType, output);
                }

                transaction.Commit();
            }
        }

        /// <summary>
        ///     Returns attributes value from specified member in config.
        ///     Unsupported types will throw exceptions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected T GetValue<T>(String name, T defaultValue = default)
        {
            CfgNode node = Root;
            foreach (var path in Path)
            {
                node = node.FindSubNode(path, false);
                if (node == null)
                {
                    return defaultValue;
                }
            }

            Object value = node.FindAttribute(name, false)?.Value;
            if (value == null)
            {
                return defaultValue;
            }

            if (typeof(T).IsEnum)
            {
                if (Enum.GetNames(typeof(T)).Contains(value.ToString()))
                {
                    return (T)Enum.Parse(typeof(T), value.ToString());
                }
            }

            if (value is Guid guid && typeof(T) != typeof(Guid))
            {
                var table = session.Tables[typeof(T)];
                if (table is GuidedTable guidedTable && guidedTable.Contains(guid))
                {
                    return (T)(Object)guidedTable[guid];
                }

                return defaultValue;
            }

            if (value is MemoBin bin)
            {
                if (typeof(T) == typeof(Byte[]))
                {
                    return (T)(Object)bin.Data;
                }
            }

            return (T)value;
        }

        /// <summary>
        ///     Internal method to get <see cref="AttributeType" /> from <see cref="Type" /> and optionally process value to
        ///     compatible - if required.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static (AttributeType, Object) GetAndProcessAttributeType<T>(T value, Object output)
        {
            if (typeof(T) == typeof(Byte[]))
            {
                output = (MemoBin)(Byte[])(Object)value;
            }

            if (typeof(T).IsEnum)
            {
                output = value.ToString();
            }

            return ((AttributeType)typeToEnumMethodInfo.Invoke(null, new Object[] { output.GetType() }), output);
        }
    }
}